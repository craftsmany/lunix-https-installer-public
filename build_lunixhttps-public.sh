#!/bin/bash
nginxver=1.15.4
wpath=/root/build
LIGHTBLUE='\033[1;34m'
GREEN='\033[0;32m'
PURPLE='\033[0;35m'
NC='\033[0m' # No Color
echo -e "*******************************************************
          ${PURPLE}craftsmany.net Lunix-HTTP/S Builder${NC}
                      ${GREEN}v1.3 - Public${NC}

 Working with NGINX Version \"${nginxver}\"
 BoringSSL with re-enabled TLS 1.3 Support
 Google PageSpeed (ngx_pagespeed)
 
 Path: ${wpath}
 BoringSSL: ${wpath}/boringssl/
 ngx_pagespeed: ${wpath}/pagespeed/
 NGINX: ${wpath}/nginx/nginx-${nginxver}/

 Copyright © 2018 Léon Tiekötter <leon@tiekoetter.com>
              https://www.craftsmany.net
*******************************************************

"
echo -e "
Create/Update Build Path"
echo -e "Do you want to delete everything in \"${wpath}/\" to start fresh?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) rm -R ${wpath}; break;;
        No ) break;;
    esac
done
echo -e "Create \"${wpath}/\" and subdirectories?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) mkdir ${wpath}/ && mkdir ${wpath}/boringssl/ && mkdir ${wpath}/pagespeed/ && mkdir ${wpath}/nginx/; break;;
        No ) break;;
    esac
done
echo -e "
${LIGHTBLUE}Install/Update Programs${NC}"
apt-get update
apt-get upgrade
apt-get install build-essential zlib1g-dev libpcre3 libpcre3-dev libssl-dev libxslt1-dev libgd-dev libgeoip-dev uuid-dev git cmake ninja-build golang
echo -e "
${LIGHTBLUE}Updating ngx_pagespeed...${NC}"
rm ${wpath}/pagespeed/build_ngx_pagespeed.sh
cd ${wpath}/pagespeed/
wget https://raw.githubusercontent.com/pagespeed/ngx_pagespeed/master/scripts/build_ngx_pagespeed.sh -P ${wpath}/pagespeed/
chmod +x ${wpath}/pagespeed/build_ngx_pagespeed.sh
./build_ngx_pagespeed.sh -b ${wpath}/pagespeed
echo -e "${LIGHTBLUE}Updating BoringSSL...${NC}"
cd ${wpath}/boringssl/
echo -e "Clone BoringSSL to \"${wpath}/boringssl/\"?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) git clone https://boringssl.googlesource.com/boringssl; break;;
        No ) break;;
    esac
done
cd ${wpath}/boringssl/boringssl/
#echo -e "
#${LIGHTBLUE}Applying BoringSSL Patch...${NC}"
#wget https://gitlab.com/buik/boringssl/raw/boringssl-patch/Enable-TLS13-BoringSSL-25-08-18.patch
#git apply Enable-TLS13-BoringSSL-25-08-18.patch
#rm Enable-TLS13-BoringSSL-25-08-18.patch
echo -e "
${LIGHTBLUE}cmake...${NC}"
mkdir build && cd build && cmake -DCMAKE_BUILD_TYPE=Release -GNinja ../
echo -e "
${LIGHTBLUE}Re-Enable TLS 1.3 draft 23 and draft 28...${NC}"
sed -i 's|tls13_rfc = 0|tls13_all = 0|' ${wpath}/boringssl/boringssl/include/openssl/ssl.h
sed -i 's|  tls13_all,|  tls13_rfc,|' ${wpath}/boringssl/boringssl/include/openssl/ssl.h
sed -i 's|tls13_variant_t tls13_variant = tls13_rfc;|tls13_variant_t tls13_variant = tls13_all;|g' ${wpath}/boringssl/boringssl/ssl/internal.h
echo -e "
${LIGHTBLUE}make (ninja)...${NC}"
ninja && cd ..
echo -e "
${LIGHTBLUE}Linking Libraries...${NC}"
mkdir -p .openssl/lib && cd .openssl && ln -s ../include . && cd ../ && cp build/crypto/libcrypto.a build/ssl/libssl.a .openssl/lib
cd ${wpath}/nginx/
echo -e "
${LIGHTBLUE}Updating NGINX...${NC}"
echo -e "Download NGINX from nginx.org to \"${wpath}/nginx/nginx-${nginxver}/\"?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) rm -R nginx-${nginxver}/ && wget http://nginx.org/download/nginx-${nginxver}.tar.gz && tar -xvzf nginx-${nginxver}.tar.gz; break;;
        No ) break;;
    esac
done
cd ${wpath}/nginx/nginx-${nginxver}/
echo -e "${LIGHTBLUE}Configure NGINX-${nginxver}...${NC}"
./configure \
    --prefix=/usr/share/nginx \
    --sbin-path=/usr/sbin/nginx \
    --conf-path=/etc/nginx/nginx.conf \
    --pid-path=/var/run/nginx.pid \
    --lock-path=/var/lock/nginx.lock \
    --error-log-path=/var/log/nginx/error.log \
    --http-log-path=/var/log/nginx/access.log \
    --user=www-data \
    --group=www-data \
    --with-http_ssl_module \
    --with-http_stub_status_module \
    --with-http_gzip_static_module \
    --with-http_v2_module \
    --with-http_geoip_module \
    --with-pcre \
    --add-module=${wpath}/pagespeed/incubator-pagespeed-ngx-latest-stable \
    --with-openssl=${wpath}/boringssl/boringssl \
    --with-openssl-opt=enable-tls1_3
touch ${wpath}/boringssl/boringssl/.openssl/include/openssl/ssl.h
echo -e "
${LIGHTBLUE}Build NGINX-${nginxver} and restart NGINX${NC}"
make && make install
wget https://cdn.craftsmany.de/source/nginx/nginx-init.txt -O ${wpath}/nginx-init.txt
cp ${wpath}/nginx-init.txt /etc/init.d/nginx
chmod +x /etc/init.d/nginx
/etc/init.d/nginx restart
echo -e "
${GREEN}Done.${NC} NGINX-${nginxver} is now active and online!"
