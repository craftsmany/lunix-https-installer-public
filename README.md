# Lunix-HTTP/S Install Script

This is the public version of the Lunix-HTTP/S install script.

Using:
- NGINX
- BoringSSL
- ngx_pagespeed
- TLS 1.3 (draft 23 and draft 28)

Variables:
- nginxver // NGINX Version to use
- wpath // Working Directory (without a "/" at the end)
